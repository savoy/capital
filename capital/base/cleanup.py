#!/usr/bin/env python3

# Capital - Financial cashflow and budgeting software
# Copyright (C) 2019 Savoy

# This file is part of Capital.
#
# Capital is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Capital is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Capital.  If not, see <http://www.gnu.org/licenses/>.

import base.sql as sql
import pandas as pd
from pathlib import Path
#import pickle5 as pickle
import pickle
import re
import shutil
import sys
from typing import List, Dict, Tuple, Union
import yaml

config = Path('~/.config/capital/config.yaml').expanduser()
state = Path('~/.config/capital/state/').expanduser()

def init(existing: str=None) -> None:
    '''Initializes the capital configuration file and database.

    Parameters
    ----------
    db_path     :   str
                The path of the already existing database, implying an import
                from another instance of Capital or a shared database.

    Raises
    ------
    FileNotFoundError   :   If a passed pre-existing database path cannot be
                            found.

    '''
    # Creation of Capital configuration directories
    if not state.is_dir():
        state.mkdir(parents=True)

    # Creation of config file if it does not exist
    if not config.is_file():
        print(f'Creating config file: {config}\n')
        shutil.copy('template_config.yaml', config)

    with config.open() as ymlfile:
        conf: dict = yaml.safe_load(ymlfile)

    # Creation of the database. If there is no database file but <db_path> is
    # passed (implying the existence of an already existing database that's
    # saved elsewhere, suggesting an import), will set the path in the config
    # to the passed path.
    db_path: Path = conf['database']['location'] / conf['database']['name']
    if existing:
        existing = Path(existing).expanduser()
        if existing.is_file():
            print(f'Setting database location: {existing.as_posix()}\n')
            conf['database']['location'] = existing.parent.as_posix()
            conf['database']['name'] = existing.name
            with existing.open('w') as f:
                yaml.dump(conf, f)
        else:
            raise FileNotFoundError(
                'The given database path cannot be found.')
    else:
        if not Path(db_path).expanduser().is_file():
            print(f'Preparing database file: {config}\n')
            with sql.Sql() as conn:
                pass

def enter() -> None:
    '''Locates an existing Capital configuration file and verifies if there
    were any vital config changes (i.e.  database file movements) compared to
    the last run or if there are any errors in the configuration.

    '''
    # Creation of Capital configuration directories
    if not state.is_dir() or not config.is_file():
        raise FileNotFoundError(
            'Please run "capital init" to set-up the program workspace.')

    # Loading of config file to compare to previous states, mostly in relation
    # to database name/location.
    with config.open() as ymlfile:
        conf: dict = yaml.safe_load(ymlfile)

    # Attempts to load the "state" file, the previous version of the Capital
    # config, to check if the database location has been changed. If it has,
    # moves it to the new location.
    try:
        with state.joinpath('config.p').open('rb') as f:
            conf_state: dict = pickle.load(f)

        db_path = Path(conf['database']['location']).joinpath(
            conf['database']['name'])
        db_state = Path(conf_state['database']['location']).joinpath(
            conf_state['database']['name'])

        if db_path != db_state:
            print(
                'Capital database has been moved. Reinitializing...\n'
                f'New database location is now {db_path}'
            )
            db_state.rename(db_path)
    except FileNotFoundError:
        pass

    # Checks to verify integrity of config options
    for cred in conf['account']['credit']:
        if cred not in conf['account']['debit']:
            raise ValueError(
                'Account config is malformed: all credit accounts should also '
                'be listed as debit accounts.'
            )

def exit() -> None:
    '''Saves the program state on exit to have verified for changes on startup.

    Will also clear out all temporary SQL tables.

    '''
    # Opens conf and saves state to keep track of previous database location
    with config.open() as ymlfile:
        conf: dict = yaml.safe_load(ymlfile)

    with state.joinpath('config.p').open('wb') as f:
        pickle.dump(conf, f, protocol=pickle.HIGHEST_PROTOCOL)

    # Gets temp table names to loop through
    with sql.Sql() as conn:
        cursor = conn.cursor()
        cursor.execute('''
            SELECT sql FROM sqlite_master WHERE type='table'
            ''')
        temp = cursor.fetchall()

    # Creates list of temp tables and clears out the data for future use
    temps = []
    for t in temp:
        try:
            temps.append(re.findall(r'TABLE (Temp\w+)', ' '.join(t))[0])
        except IndexError:
            pass
    with sql.Sql() as conn:
        cursor = conn.cursor()
        for tbl in temps:
            cursor.execute(f'DELETE FROM {tbl}')
            conn.commit()

def topkl(obj, dst: str) -> None:
    '''Takes an object to be saved as a pickle in ~/.config/capital/state/.

    In context, takes an "in-transit" transaction with the database (i.e. new
    entries to be inserted) and saves its state in case of either unforseen
    circumstances such as power loss or transport error, or if the user desires
    to do further analysis on the recent transaction entries.

    Parameters
    ----------
    obj         :   object
                Object to be pickled.

    dst         :   str
                Name of the file.

    '''
    dst: Path = state.joinpath(dst)
    with dst.open('wb') as f:
        pickle.dump(obj, f, protocol=pickle.HIGHEST_PROTOCOL)

def rmpkl(dst: str) -> None:
    '''Removes a pickle from the ~/.config/capital/state/ directory.

    Parameters
    ----------
    dst         :   str
                Name of the file.

    '''
    dst: Path = state.joinpath(dst)
    dst.unlink()

def gtpkl(dst: str, rm: bool=False) -> Union[dict, list, pd.DataFrame]:
    '''Retrieves a pickle from the ~/.config/capital/state/ directory.

    Parameters
    ----------
    dst         :   str
                Name of the file.

    rm          :   bool
                If True, removes the pickle after retrieval.

    Returns
    -------
    pkl         :   pickle
                The pickled save state.

    '''
    dst: Path = state.joinpath(dst)
    with dst.open('rb') as f:
        pkl = pickle.load(f)

    if rm:
        dst.unlink()

    return pkl
