#!/usr/bin/env python3

# Capital - Financial cashflow and budgeting software
# Copyright (C) 2019 Savoy

# This file is part of Capital.
#
# Capital is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Capital is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detail.
#
# You should have received a copy of the GNU General Public License
# along with Capital.  If not, see <http://www.gnu.org/licenses/>.

from decimal import Decimal
import os
from pathlib import Path as path
import sqlite3
import yaml

# Registering adapter and converter for Decimal values instead of floats
# Adapter: Converts the Decimal value into a string
# Converter: Takes the string in SQL and turns it back into a Decimal
sqlite3.register_adapter(Decimal, lambda r: str(r))
sqlite3.register_converter("DECIMAL", lambda c: Decimal(c.decode('utf-8')))

class Sql:
    def __init__(self):
        '''Connects to the SQL database, creating the tables and schema if they
        do not already exist.

        Usable through a "with" statement for proper entrance and cleanup.

        Attributes
        ----------
        conf            :   dict
                        Program configuration.

        db              :   str
                        Joined path of the database file taken from conf.

        conn            :   sqlite3.connection
                        Connection to the database file as specified in the
                        program's config.

        '''
        config = path('~/.config/capital/config.yaml').expanduser()

        with config.open() as ymlfile:
            self.conf = yaml.safe_load(ymlfile)

        self.db = path(self.conf['database']['location']).joinpath(
            self.conf['database']['name']).expanduser()

        if not self.db.is_file():
            raise FileNotFoundError(
                'Capital database has been moved without modifying the '
                'configuration file.'
            )

        self.conn = sqlite3.connect(
            self.db.as_posix(),
            detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES
        )
        cur = self.conn.cursor()

        cur.execute('''
            CREATE TABLE IF NOT EXISTS Inflow(
            id INTEGER PRIMARY KEY,
            date TIMESTAMP NOT NULL,
            company TEXT NOT NULL,
            detail TEXT NOT NULL,
            cost DECIMAL NOT NULL,
            category TEXT NOT NULL,
            account TEXT NOT NULL,
            finYear INTEGER NOT NULL,
            finMonth INTEGER NOT NULL,
            stdYear INTEGER NOT NULL,
            stdMonth INTEGER NOT NULL)
            ''')
        self.conn.commit()
        cur.execute('''
            CREATE TABLE IF NOT EXISTS BudgetInflow(
            id INTEGER PRIMARY KEY,
            date TIMESTAMP NOT NULL,
            category TEXT NOT NULL,
            detail TEXT NOT NULL,
            cost DECIMAL NOT NULL,
            account TEXT NOT NULL,
            finYear INTEGER NOT NULL,
            finMonth INTEGER NOT NULL,
            stdYear INTEGER NOT NULL,
            stdMonth INTEGER NOT NULL)
            ''')
        self.conn.commit()
        cur.execute('''
            CREATE TABLE IF NOT EXISTS TempInflow(
            id INTEGER PRIMARY KEY,
            date TIMESTAMP NOT NULL,
            company TEXT NOT NULL,
            detail TEXT NOT NULL,
            cost DECIMAL NOT NULL,
            category TEXT NOT NULL,
            account TEXT NOT NULL,
            finYear INTEGER NOT NULL,
            finMonth INTEGER NOT NULL,
            stdYear INTEGER NOT NULL,
            stdMonth INTEGER NOT NULL)
            ''')
        self.conn.commit()
        cur.execute('''
            CREATE TABLE IF NOT EXISTS Debit(
            id INTEGER PRIMARY KEY,
            date TIMESTAMP NOT NULL,
            company TEXT NOT NULL,
            detail TEXT NOT NULL,
            cost DECIMAL NOT NULL,
            category TEXT NOT NULL,
            account TEXT NOT NULL,
            inflow TEXT NOT NULL,
            header TEXT NOT NULL,
            finYear INTEGER NOT NULL,
            finMonth INTEGER NOT NULL,
            stdYear INTEGER NOT NULL,
            stdMonth INTEGER NOT NULL)
            ''')
        self.conn.commit()
        cur.execute('''
            CREATE TABLE IF NOT EXISTS BudgetDebit(
            id INTEGER PRIMARY KEY,
            date TIMESTAMP NOT NULL,
            category TEXT NOT NULL,
            detail TEXT NOT NULL,
            cost DECIMAL NOT NULL,
            inflow TEXT NOT NULL,
            header TEXT NOT NULL,
            finYear INTEGER NOT NULL,
            finMonth INTEGER NOT NULL,
            stdYear INTEGER NOT NULL,
            stdMonth INTEGER NOT NULL)
            ''')
        self.conn.commit()
        cur.execute('''
            CREATE TABLE IF NOT EXISTS TempDebit(
            id INTEGER PRIMARY KEY,
            date TIMESTAMP NOT NULL,
            company TEXT NOT NULL,
            detail TEXT NOT NULL,
            cost DECIMAL NOT NULL,
            category TEXT NOT NULL,
            account TEXT NOT NULL,
            inflow TEXT NOT NULL,
            header TEXT NOT NULL,
            finYear INTEGER NOT NULL,
            finMonth INTEGER NOT NULL,
            stdYear INTEGER NOT NULL,
            stdMonth INTEGER NOT NULL)
            ''')
        self.conn.commit()
        cur.execute('''
            CREATE TABLE IF NOT EXISTS Credit(
            id INTEGER PRIMARY KEY,
            date TIMESTAMP NOT NULL,
            company TEXT NOT NULL,
            detail TEXT NOT NULL,
            cost DECIMAL NOT NULL,
            category TEXT NOT NULL,
            account TEXT NOT NULL,
            payDate TIMESTAMP,
            payment DECIMAL,
            debitId INTEGER NOT NULL,
            FOREIGN KEY (debitId) REFERENCES Debit(id))
            ''')
        self.conn.commit()
        cur.execute('''
            CREATE TABLE IF NOT EXISTS Time(
            date TIMESTAMP NOT NULL,
            stdYear INTEGER NOT NULL,
            stdMonth INTEGER NOT NULL,
            finYear INTEGER NOT NULL,
            finMonth INTEGER NOT NULL)
            ''')
        self.conn.commit()
        cur.close()

    def __enter__(self):
        return self.conn

    def __exit__(self, type, value, traceback):
        self.conn.close()
