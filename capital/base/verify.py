#!/usr/bin/env python3

# Capital - Financial cashflow and budgeting software
# Copyright (C) 2019 Savoy

# This file is part of Capital.
#
# Capital is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Capital is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Capital.  If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict
import base.dates as dates
import base.sql as sql
import datetime as dt
from decimal import Decimal
from fuzzywuzzy import process
import pandas as pd
from pathlib import Path, PosixPath
import yaml
from typing import Union, Tuple, List, Dict

def config() -> dict:
    '''Returns config file.

    Returns
    -------
    conf        :   dict
                YAML configuration file dictionary.

    '''
    config = Path('~/.config/capital/config.yaml').expanduser()

    with config.open() as ymlfile:
        conf = yaml.safe_load(ymlfile)

    return conf

def acct(conf: dict, account: str=None) -> str:
    '''Sets the account as the first listed in the config file if not given.

    '''
    if not account:
        account = conf['account']['inflow'][0]
    else:
        account = account[0].upper() + account[1:].lower()
        if account not in conf['account']['inflow']:
            account = process.extract(
                account, conf['account']['inflow'])[0][0]

    return account

def is_data(data: Union[PosixPath, pd.DataFrame]) -> Union[PosixPath, pd.DataFrame]:
    '''Checks if data is either a correct CSV file path or a DataFrame.

    Parameters
    ----------
    data        :   str or pandas.DataFrame
                Path to CSV or DataFrame consisting of transactions to enter.

    Returns
    -------
    data        :   str or pandas.DataFrame
                Given data if correct format.

    Raises
    ------
    ValueError
                If str isn't a valid CSV file path or a DataFrame.

    '''
    if not Path(data).expanduser().is_file() or type(data) != pd.DataFrame:
        raise ValueError('Data must be a CSV path or a DataFrame.')
    else:
        return data

def is_table(table: str) -> str:
    '''Checks if the supplied table is editable and valid.

    Parameters
    ----------
    table       :   str
                The name of the table to be verified.

    Returns
    -------
    table       :   str
                Returns the properly capitalized table if valid.

    Raises
    ------
    ValueError
                If the table is not a valid choice.

    '''
    table_choices = ['inflow', 'debit']
    try:
        if table.lower() not in table_choices:
            raise ValueError(
                f'"{t}" is not a valid table in the database schema. '
                f'Expected one of {table_choices}'
            )
        else:
            table = table[0].upper() + table[1:].lower()
    except AttributeError:
        raise ValueError('Missing the "table" parameter.')

    return table

def is_credit(chng: pd.DataFrame) -> pd.DataFrame:
    '''Verifies if the Debit IDs were Credit transactions and if they have been
    fully paid off.

    '''
    # Prepares WHERE statement of IDs
    where = 'WHERE ( ' + ('d.id = ? OR ' * len(chng.index))[:-4] + ') '
    with sql.Sql() as conn:
        check = pd.read_sql(f'''
            SELECT d.id, d.cost, c.payment FROM Debit d
            LEFT OUTER JOIN Credit c
            ON d.id = c.debit_id
            {where}
            AND d.cost <> c.payment
            ''', conn, params=params, index_col='id')

    if check.empty():
        raise ValueError(
            'The supplied indices are Credit transactions that have already '
            'been paid and are thus unable to be modified.'
        )
    elif len(check) < len(chng.index):
        print(
            f'Only transactions {" ".join(check.id.tolist())} are being '
            'modified as the others are Credit transactions that have already '
            'been paid.'
        )
        chng = chng.drop(chng.loc[~chng.index.isin(check.id)].index)

    return chng

def categories(headers: bool=False, full: bool=False, inflow: bool=False,
               account: str=None) -> list:
    '''Pulls in the lowest level of the transaction categories as specified in
    the Capital configuration file.

    '''
    conf: dict = config()

    categories = []
    if inflow:
        if full:
            for value in conf['category']['inflow']:
                categories.append((account, value))
        elif headers:
            for value in conf['account']['inflow']:
                categories.append(value)
        else:
            for value in conf['category']['inflow']:
                categories.append(value)

    for key, value in conf['category']['constant'].items():
        if full:
            for item in value:
                categories.append((key, item))
        elif headers:
            categories.append(key)
        else:
            for item in value:
                categories.append(item)

    for key, value in conf['category']['variable'].items():
        if full:
            for item in value:
                categories.append((key, item))
        elif headers:
            categories.append(key)
        else:
            for item in value:
                categories.append(item)

    return categories

def apply_header(df: pd.DataFrame) -> pd.DataFrame:
    '''Applies the matching key to the category value.

    Parameters
    ----------
    df          :   pd.DataFrame
                The dataframe with category to be merged with the correct
                header value.

    '''
    cats = categories(full=True)
    header = pd.DataFrame(cats, columns=['header', 'category'])

    df = df.merge(header, how='left', on='category')

    return df

def check_series(series: pd.Series, check: List[str]) -> pd.Series:
    '''Runs through a pandas Series and verifying the values against the
    configuration file options with fuzzywuzzy.

    Parameters
    ----------
    series      :   pd.Series
                The DataFrame column to be acted on and verified.

    check       :   List[str]
                The configuration option value for each Series object to be
                checked against.

    Returns
    -------
    output      :   List[str]
                The cleaned-up data.

    '''
    output: list = [
        process.extract(x, check, limit=1)[0][0]
        if x not in check else x
        for x in series
    ]

    return output

def get_types(table: pd.DataFrame, convert_date: bool=False) -> pd.DataFrame:
    '''Gets the appropriate pandas dtypes and sqlite3 adapters/converters
    for each column when pulled into a DataFrame.

    Parameters
    ----------
    table       :   pd.DataFrame
                The Capital table that the output data will be used for.

    convert_date:   bool
                Will change the date columns into datetime (used if the table
                consists of string values).

    Returns
    -------
    table       :   pandas.DataFrame
                The cleaned-up table.

    '''
    # Begins the dictionary with the correct dtypes that each pandas column
    # should be.
    decimal_convert = lambda x: Decimal(x).quantize(Decimal('1.11'))
    types = {
        'dtypes'        :   {
            'date': 'datetime64', 'company': 'string', 'detail': 'string',
            'cost': 'object', 'category': 'string', 'account': 'string',
            'inflow': 'string', 'header': 'string', 'payment': 'object',
            'debitId': 'Int64'
        },
        'decimal'   :   ['cost', 'payment'],
        'date'      :   ['date', 'payDate']
    }

    # Removes any column names that aren't in the passed DataFrame so as to not
    # raise a KeyError when it tries to apply a type to a non-existant column.
    types['dtypes'] = {
        key: value for (key, value) in types['dtypes'].items()
        if key in table.columns
    }
    types['decimal'] = [
        x for x in types['decimal'] if x in table.columns
    ]

    # Sets the dtypes to their correct values, additionally converting all
    # money values to Decimal.
    table = table.astype(dtype=types['dtypes'])
    for col in types['decimal']:
        table[col] = table[col].apply(decimal_convert)

    # Sets the string valued dates to datetime
    if convert_date:
        types['date'] = [
            x for x in types['date'] if x in table.columns
        ]
        for col in types['date']:
            table[col] = pd.to_datetime(table[col], infer_datetime_format=True)

    return table

def data(table: str, data: Union[dict, PosixPath, pd.DataFrame],
         conf: dict) -> pd.DataFrame:
    '''Scrubs the given information to make sure all given items abide by type
    and choice rules.

    Parameters
    ----------
    table       :   str
                The name of the SQL table the data will be inserted into.

    data        :   object
                Can be either a pandas.DataFrame or a dict containing the data
                that needs verification.

    conf        :   dict
                The program configuration.

    Returns
    -------
    data        :   object
                The passed data.

    Raises
    ------
    ValueError
                When a value does not match what is expected
                i.e. dates not in iso, costs not number values,
                incorrect columns given.

    FileNotFoundError
                If a passed filepath does not exist.

    '''
    # Verifies whether import are standard lists, CSV file, or DataFrame,
    # defining what other params are needed and how to proceed.
    try:
        # Reads the CSV if a path is given
        if Path(data).expanduser().is_file():
            df: pd.DataFrame = pd.read_csv(data)
        else:
            raise FileNotFoundError('The given file does not exist.')
    except TypeError:
        if type(data) == dict:
            # Checks to make sure the lenth of each data list equal, as a
            # DataFrame cannot be formed otherwise (and it would mean that a
            # transaction is missing a value in a necessary column).
            n = len(data['date'])
            if not all(len(x) == n for x in data.values()):
                raise IndexError(
                    'All entered transactions must be of equal length.')
            # Prepares the DataFrame based off of the dictionary values
            df = pd.DataFrame({
                'date'      :   data['date'],
                'company'   :   data['company'],
                'detail'    :   data['detail'],
                'cost'      :   data['cost'],
                'category'  :   data['category'],
                'account'   :   data['account']
            })
            # Makes sure to add in the Inflow column if the data is Debit
            # transactions as it's a required field and its not taken care up
            # above for brevity sake.
            if table == 'Debit':
                df['inflow'] = data['inflow']
                df.inflow = df.inflow.astype('string')
        else:
            # Does nothing if the data is already a DataFrame (e.g. inputted
            # through a direct CSV temp edit).
            df: pd.DataFrame = data

    # Will make sure all values are set to their correct dtypes; will raise an
    # error if a column cannot be cast. Will also set all cost values to a
    # Decimal type and date string to datetime.
    df = get_types(df, convert_date=True)

    # table_info[0] = columns that must be in the DataFrame
    # table_info[1] = dictionary consisting of a Series name and the list to check 
    #   against with fuzzywuzzy.
    table_info = {
        'Inflow'    :   [('date', 'company', 'detail', 'cost', 'category',
                         'account'),
                         {
                             'account'  :   conf['account']['inflow'],
                             'category' :   conf['category']['inflow']
                         }],
        'Debit'     :   [('date', 'company', 'detail', 'cost', 'category',
                         'account', 'inflow'),
                         {
                             'account'  :   conf['account']['debit'],
                             'category' :   categories(),
                             'inflow'   :   conf['account']['inflow']
                         }]
    }

    # Makes sure that the column names are correct in the DataFrame (really
    # only useful for data being imported from a CSV.
    if tuple(df.columns) != table_info[table][0]:
        raise ValueError(
            'The df column names are incorrect. Please correct and try '
            'inserting the transaction(s) again.'
        )

    # Goes through the long process of making sure the inputted values are
    # correct and match the categories/accounts in the configuration file.
    for key, value in table_info[table][1].items():
        df[key] = check_series(df[key], value)

    # Applies headers to each category if modifying Debit
    if table == 'Debit':
        df = apply_header(df)

    # Cycles through the date column to create the four additional splits
    for c in ('finYear', 'finMonth', 'stdYear', 'stdMonth'):
        df[c] = dates.vector_calendar(df.date, c)

    return df

def main_init(date: str, account: str=None) -> Tuple[
        dict, str, str,
        Tuple[dt.date, dt.date, Union[int, dt.date]],
        List[Union[dt.date, str]]]:
    '''General initialization steps for Output & Modify due to their
    similarity.

    Parameters
    ----------
    date        :   str
                The string-formatted date (YYYY-MM or YYYY)

    account     :   str
                The name of the Inflow account.

    Returns
    -------
    output      :   Tuple[dict, str, str, list, list]
                Consisting of the configuration file, the Inflow account
                to be used, the column name of months that indiciate what
                date style will be used (financial v standard), the date range
                in datetime.date format, and the parameters used for SQL.

    Raises
    ------
    ValueError  :   If the passed date is not in the correct format.

    '''
    conf: dict = config()

    # If account not passed, chooses the first
    account: str = acct(conf, account)

    # Sets datetime type based on the configuration set.
    if conf['datetime'] == 'financial':
        months = 'finMonth'
    else:
        months = 'stdMonth'

    # Date verification and pulling the correct date range (the dates library
    # already pulls in the configuration file).
    if len(date) == 4:
        cal: Tuple[dt.date, dt.date, Union[int, dt.date]] = dates.calendar(
            int(date))
    elif len(date) == 7:
        cal: Tuple[dt.date, dt.date, Union[int, dt.date]] = dates.calendar(
            int(date[:4]), int(date[5:]))
    else:
        raise ValueError(
            'Date value must be either YYYY or YYYY-MM'
        )

    params = cal[0:2] + (account, )

    return conf, account, months, cal, params
