#!/usr/bin/env python3

# Capital - Financial cashflow and budgeting software
# Copyright (C) 2019 Savoy

# This file is part of Capital.
#
# Capital is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Capital is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Capital.  If not, see <http://www.gnu.org/licenses/>.

import base.sql as sql
import calendar as calutil
from collections import defaultdict
import datetime as dt
from dateutil.relativedelta import relativedelta
import pandas as pd
from pathlib import Path
import yaml
from typing import List, Union, Tuple, Dict

def _daterange(start: dt.date, end: dt.date, delta=relativedelta(months=1)):
    '''Iterates through a desired start and end date by the given delta.

    Parameters
    ----------
    start       :   datetime.date
                Start date for the iteration.

    end         :   datetime.date
                End date for the iteration. If iterating over months, make the
                day value higher compared to start to include the month in the
                iteration.

    delta       :   dateutil.relativedelta
                A relative time delta value to iterate as.

    '''
    while start < end:
        yield start
        start += delta


def monthcount(d_rng: Tuple[dt.date, dt.date], detail: bool = False,
               year: bool = False) -> Dict[
                   Union[int, dt.date], Union[int, List[dt.date]]]:
    '''Counts the number of days in each month present in the month (does not
    yet use accounting month.

    The month with the highest len() is accounting month for the specified
    range.

    Parameters
    ----------
    d_rng           :   Tuple[datetime.date, datetime.date]
                    List containing the start and end date of an accounting
                    month.

    detail          :   bool
                    Outputs the range for each month instead of just the
                    number of days.

    year            :   bool
                    Will output the number of days in the year instead of
                    each month.

    Returns
    -------
    bkdn            :   dict
                    Breakdown of each year/month combination along with its
                    quantity in the date range. Only returned over acc_month if
                    detail=True.

    Raises
    ------
    ValueError
                    When one of the supplied values in d_rng is not a datetime.

    IndexError
                    When the length of d_rng contains more than 2 values.

    '''
    bkdn = defaultdict(int)
    start = d_rng[0]
    end = d_rng[1] + relativedelta(days=1)

    for dat in _daterange(start, end, delta=relativedelta(days=1)):
        if year:
            bkdn[dat.year] += 1
        else:
            bkdn[dt.date(dat.year, dat.month, 1)] += 1

    if detail:
        deets = {}
        temp = [relativedelta(days=0)]
        for key, value in bkdn.items():
            days = relativedelta(days=sum([x.days for x in temp]))
            first = d_rng[0] + days
            last = first + relativedelta(days=(value - 1))
            deets[key] = [first, last]
            temp.append(relativedelta(days=value))
        return deets
    else:
        return bkdn


def _cal_helper(year: int, month: int) -> List[dt.date]:
    '''Calculates the correct accounting date range for the given month/year.

    Split from calendar() as that performs the additional check with a
    reference date, allowing it to loop through cal_helper() if it's not in the
    range.

    Parameters
    ----------
    year        :   int
                An integer value signifying year.

    month       :   int
                An integer value signifying a month of the year.

    Returns
    -------
    acc_cal     :   list [datetime.date]
                The correct accounting month calendar range for the given
                month.

    '''
    # Start date of stuff
    bigbang = dt.date(1969, 12, 28)

    # Detailing which months have four vs five weeks, including the six week
    # special
    four = [1, 2, 4, 5, 7, 8, 10, 11]
    five = [3, 6, 9, 12]
    six = [dt.date(1970, 12, 1), dt.date(1976, 12, 1), dt.date(1981, 12, 1),
           dt.date(1987, 12, 1), dt.date(1992, 12, 1), dt.date(1998, 12, 1),
           dt.date(2004, 12, 1), dt.date(2009, 12, 1), dt.date(2015, 12, 1),
           dt.date(2020, 12, 1), dt.date(2026, 12, 1), dt.date(2032, 12, 1),
           dt.date(2037, 12, 1), dt.date(2043, 12, 1), dt.date(2048, 12, 1),
           dt.date(2054, 12, 1), dt.date(2060, 12, 1), dt.date(2065, 12, 1),
           dt.date(2071, 12, 1), dt.date(2076, 12, 1), dt.date(2082, 12, 1),
           dt.date(2088, 12, 1), dt.date(2093, 12, 1), dt.date(2099, 12, 1)]

    four_weeks = 28
    five_weeks = 35
    six_weeks = 42

    std_year = 364
    leap_year = 371

    # Finds months by cycling through since the start date
    count = 0
    four_count = 0
    five_count = 0
    six_count = 0
    if month != 0:
        for dat in _daterange(bigbang, dt.date(year, month, 1),
                             delta=relativedelta(months=1)):
            count += 1
            if count > 1:
                if dt.date(dat.year, dat.month, 1) in six:
                    six_count += 1
                elif dat.month in four:
                    four_count += 1
                elif dat.month in five:
                    five_count += 1
        days_since = (four_count * four_weeks) + (five_count * five_weeks) + (
            six_count * six_weeks)
    else:
        for dat in _daterange(bigbang, dt.date(year, 1, 1),
                             delta=relativedelta(years=1)):
            count += 1
            if count > 1:
                if dt.date(dat.year, 12, 1) in six:
                    five_count += 1
                else:
                    four_count += 1
        days_since = (four_count * std_year) + (five_count * leap_year)

    # Find the amount of days since the big bang to accurately place what
    # the accounting month range is for the supplied year/month combo.
    start = bigbang + dt.timedelta(days=days_since)
    if month != 0:
        if dt.date(year, month, 1) in six:
            end = start + dt.timedelta(days=(six_weeks - 1))
        elif month in four:
            end = start + dt.timedelta(days=(four_weeks - 1))
        elif month in five:
            end = start + dt.timedelta(days=(five_weeks - 1))
    else:
        if dt.date(year, 12, 1) in six:
            end = start + dt.timedelta(days=(leap_year - 1))
        else:
            end = start + dt.timedelta(days=(std_year - 1))

    acc_cal = [start, end]
    return acc_cal


def calendar(year: int, month: int = 0, ref: dt.date = None,
             string: bool = False,
             rng: bool = False) -> List[Union[dt.date, str, int]]:
    '''Outputs a date range of the correct accounting year or month given those
    values as a reference point.

    Parameters
    ----------
    year        :   int
                An integer value signifying year.

    month       :   int
                An integer value signifying a month of the year.

    ref         :   datetime.date
                The reference date. If present, will check if it falls
                between the date range values set by $year and $month,
                outputting the correct month range if not.

    conf        :   bool
                If True (default), will abide by the 'datetime' configuration
                option under ~/.config/capital/config.yaml.

    Returns
    -------
    acc_cal     :   List[Union[dt.date, str, int]]
                The correct accounting month calendar range for the given
                month as well as the datetime of year/month linked to that
                range.

    '''
    # Checks if date given is out of range
    if year < 1970 or year > 2099:
        raise ValueError(
            'Year value out of range.'
        )

    # Gets the accounting date range from cal_helper()
    acc_cal = _cal_helper(year, month)

    # If a reference date is supplied and is not in the calculated range,
    # the correct accounting range for the reference date is given.
    if ref and not acc_cal[0] <= ref <= acc_cal[1]:
        days_away = ref - acc_cal[0]
        if days_away.days > 0:
            rng_fix = (acc_cal[0], ref + relativedelta(months=1))
        if days_away.days < 0:
            rng_fix = (ref - relativedelta(months=1), acc_cal[0] + relativedelta(months=1))

        month_count = monthcount(rng_fix)
        for dat in reversed(list(month_count.keys())):
            acc_cal = _cal_helper(dat.year, dat.month)
            if acc_cal[0] <= ref <= acc_cal[1]:
                if month == 0:
                    acc_cal.append(dat.year)
                else:
                    acc_cal.append(dt.date(dat.year, dat.month, 1))
                break

    else:
        if month == 0:
            acc_cal.append(year)
        else:
            acc_cal.append(dt.date(year, month, 1))

    # Converts calendar output to ISO string if flagged
    if string:
        for index, dat in enumerate(acc_cal):
            try:
                acc_cal[index] = calendar(dat.year, dat.month, ref=dat)[2].strftime('%Y-%m')
            except AttributeError:
                acc_cal[index] = str(dat)

    if rng:
        acc_cal = acc_cal[:2]

    return acc_cal


def close(ytd: bool = False, delta: int = 0, string: bool = False,
          rng: bool = False, num: bool = False) -> List[
              Union[dt.date, str, int]]:
    '''Outputs the most recent start/end accounting close calendar range.

    Can either output the most recently closed month or YTD to the most
    recently closed month (along with X number of years past).

    Parameters
    ----------
    ytd     :   bool
            Changes the output range from current month close to YTD up to
            current month close.

    delta   :   bool
            Used in tandem with <ytd>, goes X number of years back.

    string  :   bool
            Outputs the data in string ISO format.

    rng     :   bool
            Outputs only the date range (first two indices), excluding the
            third which indicates the month in datetime.date format.

    num     :   bool
            Outputs the data in int format (%Y%m%d).

    Returns
    -------
    calendarendar   :   List[Union[dt.date, str, int]]
                The correct accounting close calendar range for the most
                recent month as well as the datetime of year/month linked to
                that range.

    '''
    today = dt.date.today()
    tyear = today.year
    tmo = today.month

    calendarendar = calendar(tyear, tmo)

    if today <= calendarendar[1]:
        yest = today - relativedelta(months=1)
        calendarendar = calendar(yest.year, yest.month)

    if ytd:
        year = calendar(calendarendar[2].year - delta, 1)

        calendarendar[0] = year[0]

    if string:
        for index, dat in enumerate(calendarendar):
            try:
                calendarendar[index] = calendar(
                    dat.year, dat.month, ref=dat)[2].strftime('%Y-%m')
            except AttributeError:
                calendarendar[index] = str(dat)
    elif num:
        for index, dat in enumerate(calendarendar):
            calendarendar[index] = int(
                dat.strftime('%Y') + dat.strftime('%m') + dat.strftime('%d'))

    if rng:
        calendarendar = calendarendar[:2]

    return calendarendar


def vector_calendar(series: pd.Series, column: str) -> pd.Series:
    output: list = [
        calendar(x.year, x.month, ref=x.date())[2]
        if column in ('finYear', 'finMonth') else x
        for x in series
    ]
    output = [
        x.year if column in ('finYear', 'stdYear')
        else x.month for x in output
    ]

    return output


def to_sql() -> None:
    '''Sets up the Time table in SQL for fast date access through 2099.

    '''
    date = []
    for x in dates.daterange(dt.date(1969,12,28), dt.date(2100,1,2),
                             delta=relativedelta(days=1)):
        date.append(x)

    time = pd.DataFrame({'date': date})
    time['stdYear'] = pd.DatetimeIndex(time['date']).year
    time['stdMonth'] = pd.DatetimeIndex(time['date']).month
    time['finYear'] = time['date'].map(
        lambda x: calendar(x.year, x.month, ref=x)[2].year)
    time['finMonth'] = time['date'].map(
        lambda x: calendar(x.year, x.month, ref=x)[2].month)

    with sql.Sql() as conn:
        time.to_sql('Time', conn, if_exists='replace')

