#!/usr/bin/env python3

# Capital - Financial cashflow and budgeting software
# Copyright (C) 2019 Savoy

# This file is part of Capital.
#
# Capital is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Capital is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detail.
#
# You should have received a copy of the GNU General Public License
# along with Capital.  If not, see <http://www.gnu.org/licenses/>.

import datetime as d
from dateutil.relativedelta import relativedelta
import base.cleanup as cleanup
import base.dates as dates
import base.sql as sql
import base.verify as verify
import interface.csv as csv
from fuzzywuzzy import process
import numpy as np
import pandas as pd
from pathlib import Path
import sqlite3
from typing import List, Dict, Tuple, Union
import re
import yaml

# As printing the output into a terminal is a main feature, makes sure to
# display all rows when necessary.
pd.set_option('display.max_rows', 1000)

def prev_delta(cal, frame: str='m', delta: int=0,
               budget: bool=True) -> Tuple[dict, dict]:
    '''Pulls pre-existing data in reference to cal, which is in turn generated
    from dates.calendar().

    Parameters
    ----------
    frame       :   str
                The timeframe to compare the transactions to, either vs a
                month ('m'), month over years ('moy'), or a year ('y').

    delta       :   int
                The amount of frame units to compare the transactions to
                e.g. a month frame with a delta of 2 will look at the
                current month as well as the same month from the last two
                years.

    budget      :   bool
                Will also show the budget numbers in accordance to the
                choices set with FRAME and DELTA.

    Returns
    -------
    deltas      :   dict [pandas.DataFrame]
                A variable amount (based on DELTA) of DataFrames for the
                chosen FRAMEs and category list in the inserted
                transactions.

    budgets     :   dict [pandas.DataFrame]
                A variable amount (based on DELTA) of DataFrames for the
                budgets of the chosen FRAMEs and category list in the
                inserted transactions.

    '''
    # Sets the dictionaries that compile all of the dataframes
    deltas = {}
    budgets = {}

    # Iterates through the range given, finding the start and end date of each
    # month or year and pulling the information from SQL to add to the output.
    with sql.Sql() as conn:
        for d in range(0,delta+1):
            if frame == 'm':
                if d > 0:
                    dt = cal[2] - relativedelta(months=1*d)
                    dt_range = dates.calendar(dt.year, dt.month)
                else:
                    dt_range = cal
            elif frame == 'moy':
                if d > 0:
                    dt = cal[2] - relativedelta(months=12*d)
                    dt_range = dates.calendar(dt.year, dt.month)
                else:
                    dt_range = cal
            elif frame == 'y':
                dt = cal[2] - relativedelta(years=1*d)
                dt_start = dates.calendar(dt.year, 1)[0]
                dt_end = dates.calendar(dt.year, 12)[1:]
                dt_range = [dt_start, dt_end[0], dt_end[1]]

            dtf = pd.read_sql(
                f'''
                SELECT * FROM {self.table} WHERE
                date <= ? AND date >= ?
                ''', conn, params=[
                    d.date(dt_range[1].year, dt_range[1].month,
                           dt_range[1].day),
                    d.date(dt_range[0].year, dt_range[0].month,
                           dt_range[0].day)
                ]
            )
            deltas[dt_range[2]] = dtf

            if budget:
                dtb = pd.read_sql(
                    f'''
                    SELECT * FROM Budget{self.table} WHERE
                    date <= ? AND date >= ?
                    ''', conn, params=[
                        d.date(dt_range[1].year, dt_range[1].month,
                               dt_range[1].day),
                        d.date(dt_range[0].year, dt_range[0].month,
                               dt_range[0].day)
                    ]
                )
                budgets[dt_range[2]] = dtb

    return deltas, budgets

def current(account: str=None) -> pd.DataFrame:
    '''Returns current bank balance along with pending Credit payments.

    Parameters
    ----------
    account         :   str
                    The name of the Inflow account to pull the data for.

    Returns
    -------
    current         :   pandas.DataFrame
                    The bank balance for $account, the total pending Credit
                    payments, and the net capital balance.

    '''
    # Gets the list of credit accounts from the Capital configuration file.
    conf: dict = verify.config()
    account: str = verify.acct(conf, account)
    credits: List[str] = conf['account']['credit']

    # If credit accounts exist in the configuration, prepares the SQL statement
    # to pull the data.
    if credits:
        where = ' AND (' + ('account = ? OR ' * len(credits))[:-4] + ')'
    else:
        where = ''

    # Runs the SQL queries
    with sql.Sql() as conn:
        cur = conn.cursor()
        # Gets the net total of capital, selecting the total Inflow subtracted
        # by the total costs (in Debit, as costs in Credit are just the balance
        # for whats left to pay).
        capital: pd.DataFrame = pd.read_sql('''
            SELECT SUM(cost) -
            (SELECT SUM(cost) FROM Debit WHERE inflow=?) as cost
            FROM Inflow WHERE account=?
            ''', conn, params=[account]*2)
        capital = verify.get_types(capital)
        # Sets the sum total to the "Capital" account
        capital['account'] = 'Capital'
        capital = capital[['account', 'cost']]

        # Gets the outstanding Credit payments to display
        credit: pd.DataFrame = pd.read_sql(f'''
            SELECT account, SUM(cost) as cost
            FROM Credit WHERE payment IS NULL{where}
            GROUP BY account
            ''', conn, params=credits)
        credit = verify.get_types(credit)

        # Finalizes the DataFrame, adding an additional row with the gross
        # value, which is simply the net plus the outstanding credit payments.
        initial = pd.DataFrame(
            {'account'  :   'Initial',
             'cost'     :   [capital.cost.item() + credit.cost.sum()]
             })
        current: pd.DataFrame = initial.append(credit).append(
            capital).reset_index(drop=True)

    return current

class Output:
    '''Concerns itself with the retrieval of transactions with the option of
    showing a summary as well.

    '''
    def __init__(self, date: str, account: str=None) -> None:
        '''Prepares the data slice based on the given criteria.

        Parameters
        ----------
        date        :   str
                    Timeframe of the data to view. Should be either YYYY or
                    YYYY-MM.

        account     :   str
                    Inflow account to view. If None, uses the first item in
                    your configuration.

        Attributes
        ----------
        conf        :   dict
                    The program configuration.

        months      :   str
                    The calendar type as specified in the configuration
                    (financial vs standard)3

        cal         :   list [datetime.date]
                    The month range of the current date based on the accounting
                    calendar.

        inflow      :   pandas.DataFrame
                    Inflow line items for the given account and date.

        debit       :   pandas.DataFrame
                    Debit line items for the given account and date.

        credit      :   pandas.DataFrame
                    Credit line items for the given account and date.

        outflow     :   pandas.DataFrame
                    Merged debit/credit frame to essentially show debit with
                    payDate and payment for any credit transactions.

        df          :   pandas.DataFrame
                    Frame of both the inflow and outflow tables.

        Raises
        ------
        ValueError
                    When a parameter value does not match what is expected
                    i.e. dates not in iso.

        '''
        # Loads configuration file and sets attributes
        self.conf, self.account, self.months, self.cal, params = verify.main_init(date, account)

        # Pulls the SQL tables for all 3 buckets: Inflow, Debit, and Credit
        # based on the criteria in <params> (start/end date and Inflow
        # account).
        with sql.Sql() as conn:
            self.inflow: pd.DataFrame = pd.read_sql('''
                SELECT * FROM Inflow
                WHERE date >= ? AND date <= ? AND account = ?
                ''', conn, params=params)
            self.debit: pd.DataFrame = pd.read_sql('''
                SELECT * FROM Debit
                WHERE date >= ? AND date <= ? AND inflow = ?
                ''', conn, params=params)
            # Credits are set to negative to offset Debit and filtered not by
            # the given date but by the IDs in the Debit table as the dates
            # may not always match up (and the following merging to match up
            # each Credit to it's respective Debit.
            self.debit['cost'] = self.debit['cost'] * -1
            self.credit: pd.DataFrame = pd.read_sql(f'''
                SELECT * FROM Credit
                WHERE debitId >= ? AND debitId <= ?
                ''', conn, params=(self.debit.id.min(), self.debit.id.max()))

        # Joining all 3 tables together, merging the Debit and Credit tables on
        # ID so as to see if a Debit has a matching Credit and its payment
        # status.
        self.outflow = self.debit.merge(self.credit.drop(columns=list(
            self.debit.columns[:7])), how='left', left_on='id',
                                      right_on='debitId')
        self.df = self.inflow.append(self.outflow, ignore_index=True,
                                  sort=False)

        # Sets the header column (the first level category for debits e.g.
        # Entertainment -> Technology) for Debit rows, and to the account for
        # Inflow.
        self.df['header'] = np.where(self.df['header'].isna(),
                                          self.account, self.df['header'])

        # Sets all column types to their correct values
        self.df = verify.get_types(self.df)

    def summary(self, detail: bool=False) -> pd.DataFrame:
        '''Returns a summarized view of the financial data.

        Parameters
        ----------
        detail      :   bool
                    To show category details instead of summed up by header.
                    Only relevant if showing transactions.

        Returns
        -------
        table       :   pandas.DataFrame
                    Summary of financials.

        '''
        # Prepares output based on style
        if detail:
            # Detail will show costs by category instead of just header.
            # However, the feature is YET TO BE FULLY FINALIZED as the sorting
            # still requires some work.
            table = pd.pivot_table(self.df, values=['cost'], index=['header', 'category'],
                                   columns=[self.months], aggfunc='sum', margins=True)
            sorter: List[Tuple[str, str]] = verify.categories(
                inflow=True, full=True, account=self.account)
            sorter = [x for x in sorter if x in table.index]
            sorter.append(('All', ''))
            tbl = table.groupby(level=0).sum()
            tbl.index = pd.MultiIndex.from_arrays(
                [tbl.index.values, len(tbl.index) * [''],
                 len(tbl.index) * ['']]
            )
            table = pd.concat([table, tbl]).sort_index(level=[0])
            #table = table.reindex(index=sorter)
            ranker = 'category'

        else:
            # Unlike above, will simply summarize by the header, sorting by the
            # already present sorting in the configuration file.
            table = pd.pivot_table(
                self.df, index=['header'], columns=[self.months],
                values=['cost'], aggfunc='sum', margins=True)
            sorter: List[Tuple[str, str]] = verify.categories(
                headers=True, inflow=True)
            sorter = [x for x in sorter if x in table.index]
            sorter.append('All')
            ranker = 'header'

        # ONLY WORKS IF NOT DETAIL
            table = table.reindex(index=sorter)

        return table

    def trans(self, n: int=0) -> pd.DataFrame:
        '''Returns the list of transactions.

        Parameters
        ----------
        n           :   int
                    The number of transaction lines to output.

        Returns
        -------
        table       :   pandas.DataFrame
                    List of transactions.

        '''
        # Drops unneeded columns (year/month individuals) as it will just be a
        # list of all transactions.
        table = self.df.drop(
            columns=['finYear', 'finMonth', 'stdYear',
                    'stdMonth']).sort_values(
                        by=['date', 'id'], ascending=[True, True])
        if n != 0:
            table = table.tail(n=n)

        return table

class Modify:
    '''Concerns itself with the retrieval and potential manipulation of Capital
    data.

    '''
    def __init__(self, date: str, table: str, account: str=None, index:
                 list=None):
        '''Prepares the data slice based on the given criteria.

        Parameters
        ----------
        date        :   str
                    Timeframe of the data to view. Should be either YYYY or
                    YYYY-MM.

        table       :   str
                    Name of the transaction table to pull.

                    Inflow:    Revenue, income, etc.
                    Debit:     Debit account, bank payments, auto charges.

        account     :   str
                    Inflow account to view. If None, uses the first item in
                    your configuration.

        index       :   list
                    List of Debit IDs to modify. If left None, will exit unless
                    modifying with the --edit CLI argument.

        Attributes
        ----------
        conf        :   dict
                    The program configuration.

        cal         :   list [datetime.date]
                    The month range of the current date based on the accounting
                    calendar.

        data        :   pandas.DataFrame
                    Table of items being pulled to be modified.

        cols        :   list
                    Editable columns.

        Raises
        ------
        ValueError
                    When a parameter value does not match what is expected
                    i.e. dates not in iso.

        '''
        # Editable columns
        self.cols = ['id', 'date', 'company', 'detail', 'cost', 'category',
                     'account']
        # Loads configuration file and sets attributes
        self.table = verify.is_table(table)
        if self.table == 'Inflow':
            col = 'account'
        else:
            col = 'inflow'
            self.cols.extend(['inflow'])

        self.conf, self.account, self.months, self.cal, params = verify.main_init(date, account)

        # Prepares parameter of IDs if given
        if index:
            if not all([type(x) == int for x in index]):
                for key, value in enumerate(index):
                    index[key] = int(value)
            params.extend(index)
            where = ' AND (' + ('id = ? OR ' * len(index))[:-4] + ')'
        else:
            where = ''

        # Pulls the SQL tables
        with sql.Sql() as conn:
            self.data = pd.read_sql(f'''
                SELECT {', '.join(self.cols)} FROM {self.table}
                WHERE date >= ? AND date <= ? AND {col} = ?{where}
                ''', conn, params=params, index_col='id')

        # Gets index and converts date to ISO string (to compare to update)
        self.index = self.data.index
        self.data.date = self.data.date.astype(str)

    def modify(self, data=None, edit: bool=False) -> None:
        '''Modifies currently existing transactions.

        Parameters
        ----------

        data        :   pd.DataFrame
                    The values to be changed. DataFrame self.data with the
                    changes.

        edit        :   bool
                    If True, will load a temp CSV to easily load all
                    information to be modified.

        '''
        # Checks if modified data is present
        if edit:
            self.update = csv.modify(self.data)
        else:
            self.update = verify.is_data(data)

        # Verifies data and creates DataFrame from CSV if necessary
        self.update = verify.data(self.table, self.update, self.conf)
        try:
            self.update.index=self.index
        except ValueError:
            raise ValueError('Do not add/delete transactions through modify.')

        # Finds unchanged values to not overwrite in the database
        unchg = pd.merge(self.data.reset_index(),
                              self.update.loc[:, self.cols[1:]], how='inner',
                              on=self.cols[1:]).set_index('id').index

        self.chng = self.update.drop(index=unchg)

        # Checks to see if the modified items are Credit transactions, and if
        # they have been paid, raises an error.
        if self.table == 'Debit':
            self.chng = verify.is_credit(self.chng)

        # Push modifications to temp table and update the main table with the
        # changes.
        if self.table == 'Debit':
            debit_col = (f'''
                SET inflow=(
                    SELECT tmp.inflow FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                SET header=(
                    SELECT tmp.header FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
            ''')
        with sql.Sql() as conn:
            self.chng.to_sql(f'Temp{self.table}', conn, if_exists='append')
            cursor = conn.cursor()
            cursor.execute(f'''
                UPDATE {self.table}
                SET date=(
                    SELECT tmp.date FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                SET company=(
                    SELECT tmp.company FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                SET detail=(
                    SELECT tmp.detail FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                SET cost=(
                    SELECT tmp.cost FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                SET category=(
                    SELECT tmp.category FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                SET account=(
                    SELECT tmp.account FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                {debit_col}
                SET finYear=(
                    SELECT tmp.finYear FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                SET finMonth=(
                    SELECT tmp.finMonth FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                SET stdYear=(
                    SELECT tmp.stdYear FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                SET stdMonth=(
                    SELECT tmp.stdMonth FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id),
                WHERE id=(
                    SELECT id FROM Temp{self.table} tmp
                    WHERE {self.table} = tmp.id)
                ''')
            conn.commit()

class Insert:
    def __init__(self, table, data=None, edit: bool=True, date: List[str]=None,
                 company: List[str]=None, detail: List[str]=None,
                 cost: List[float]=None, category: List[str]=None,
                 account: List[str]=None, inflow: List[str]=None):
        '''Prepares to insert new transactions into the Capital database.

        One type of transaction is inputted at once, set by "table". Each index
        of the following lists corresponds to one line item e.g. date[0],
        company[0], and cost[0] are all the same transaction.

        Parameters
        ----------
        table       :   str
                    Name of the transaction table for the line items to be
                    entered into.

                    Inflow:    Revenue, income, etc.
                    Debit:     Debit account, bank payments, auto charges.

        data        :   object
                    The actual data to be passed into Capital. Only needs to
                    be specified if column parameters are not being passed. Can
                    either be a string path to a properly formatted CSV, a
                    pandas DataFrame, or None if edit is True.

        edit        :   bool
                    If True, will load a temp CSV to easily load all
                    information.

        date        :   list
                    The point of contact date of the transaction. Date
                    strings must be in a YMD format or be a datetime
                    object.

                        YYYY-MM-DD, YY-MM-DD, YYYYMMDD, YYMMDD

        company     :   list
                    The other party in the transaction e.g. company
                    purchased from, individual who transferred money.

        detail     :   list
                    Additional detail to further categorize the
                    transaction. Could be items bought, information on the
                    company, etc.

        cost        :   list
                    Money value of the transaction.

        category    :   list
                    The group the transaction will fit under. The full
                    list is modifiable and can be found in the config file
                    under database properties.

        account     :   list
                    Either an inflow account (debit account, savings) or a
                    debit account (bank card, credit card). The full list is
                    modifiable and can be found in the config file under
                    database properties.

        inflow      :   list
                    Relevant only for Debit transactions. Links the cost to an
                    Inflow account e.g. one transaction can go against a joint
                    bank account while another against a personal one.

        Attributes
        ----------
        conf        :   dict
                    The program configuration.

        table       :   str
                    Name of the transaction table for the line items to be
                    entered into.

        df          :   pandas.DataFrame
                    An organized DataFrame of new transactions to enter
                    into the database or analyze.

        credits     :   pandas.DataFrame
                    If table is set to "Debit", a duplicate is created
                    with the additional (currently blank) Series "payDate"
                    and "payment".

        current     :   datetime.date
                    Today's date.

        cal         :   list [datetime.date]
                    The month range of the current date based on the accounting
                    calendar.

        Raises
        ------
        ValueError
                    When a parameter value does not match what is expected
                    i.e. dates not in iso, costs not number values,
                    incorrect table chosen.

        '''
        # Verifies whether the table name used is a valid choice and matches
        # the case with the correct format as matches the SQL table.
        self.table = verify.is_table(table)

        # Loads configuration file
        self.conf: dict = verify.config()

        # If all individual parameters are passed, the data is converted into a
        # dictionary to be parsed and verified. Otherwise, the CSV path or the
        # DataFrame itself is passed.
        items = (date, company, detail, cost, category, account)
        if all(item is None for item in items):
            if edit is True:
                # By default will open a temp CSV to edit, pulling in the data
                data = csv.insert(table)
            else:
                # Otherwise, runs a check to make sure the passed data (i.e.
                # the path) points to an existing data set.
                data = verify.is_data(data)
        else:
            # No data is passed, so will go by the individual arguments.
            if any(item is None for item in items):
                raise ValueError(
                    'Missing parameters. Please include all information.')
            elif self.table == 'Inflow':
                data = {
                    'date'      :   date,
                    'company'   :   company,
                    'detail'    :   detail,
                    'cost'      :   cost,
                    'category'  :   category,
                    'account'   :   account
                }
            elif self.table == 'Debit':
                if not inflow:
                    raise ValueError('Missing inflow data.')
                data = {
                    'date'      :   date,
                    'company'   :   company,
                    'detail'    :   detail,
                    'cost'      :   cost,
                    'category'  :   category,
                    'account'   :   account,
                    'inflow'    :   inflow
                }

        # Cleaning of the user-inputted data, making sure everything matches
        # out correctly to be inserted into SQL.
        self.df: pd.DataFrame = verify.data(self.table, data, self.conf)

        # Sets current date information
        self.current = d.date.today()
        self.cal = dates.calendar(self.current.year, self.current.month,
                                    ref=self.current)

        # Prepares insert save state
        to_pickle = [self.df]

        # Checks which table the transactions will be entered into; adds the
        # additional DataFrame for Credit if needed, removing superfluous
        # columns (that will just be added again later) and dropping any
        # non-credit accounts (i.e. Debit).
        if self.table == 'Debit':
            self.credits = self.df.copy().drop(
                columns=['inflow', 'header', 'finYear', 'finMonth', 'stdYear',
                         'stdMonth'])
            self.credits = self.credits.drop(
                self.credits.loc[~self.credits['account'].isin(
                    self.conf['account']['credit'])].index).reset_index(
                        drop=True)
            self.credits['payDate'] = None
            self.credits['payment'] = None
            to_pickle.append(self.credits)

        # Pickles save state transactions
        cleanup.topkl(to_pickle, 'insert.p')

    def insert(self) -> None:
        '''Inserts the transaction DataFrame self.df into its correct SQL
        table.

        Insertion occurs through pandas. If the transaction list is designated
        for Debit, a duplicate DataFrame is also inserted into the Credit
        table with the additional blank Series appended (i.e. payDate and
        payment).

        '''
        if self.table == 'Debit':
            # Gets the number of Credit transactions for use in pulling the
            # correct amount further down.
            trans: int = len(self.df.loc[
                    self.df.account.isin(self.conf['account']['credit']) ])
            # Sets up the SQL statement of accounts to pull to get the correct
            # transactions to make Credit offsets for.
            accounts: str = 'account = ? OR ' * len(self.conf['account']['credit'])
            with sql.Sql() as conn:
                # Pushes the transactions to SQL
                self.df.to_sql(self.table, conn, if_exists='append',
                               index=False)
                # Uses the previous two objects created to assist in pulling
                # all of auto-generated IDs for the transactions just inserted,
                # as they will be needed to insert the offsetting Credit
                # transactions and match up.
                debits: pd.DataFrame = pd.read_sql(f'''
                    SELECT
                    id FROM Debit
                    WHERE {accounts[:-3]}
                    ORDER BY id DESC LIMIT {trans}
                    ''', conn, params=self.conf['account']['credit'])
                self.credits['debitId'] = debits['id'].sort_values(
                        ).reset_index(drop=True)
                try:
                    self.credits.to_sql('Credit', conn, if_exists='append',
                                        index=False)
                except sqlite3.IntegrityError:
                    pass

        elif self.table == 'Inflow':
            # Inflow insertion is simple and easy as there are no balancing
            # Credit transactions to worry about.
            with sql.Sql() as conn:
                self.df.to_sql(self.table, conn, if_exists='append',
                               index=False)

    def compare(self, frame: str='m', delta: int=0, budget: bool=True):
        '''Compares the transaction list to pre-existing data and pulls general
        statistics.

        By category of the transactions, will compare the inserted values'
        average, median, mode, and % of total on a monthly or yearly basis,
        with the option of viewing it compared to the budget as well.

        Parameters
        ----------
        frame       :   str
                    The timeframe to compare the transactions to, either vs a
                    month ('m'), month over years ('moy'), or a year ('y').

        delta       :   int
                    The amount of frame units to compare the transactions to
                    e.g. a month frame with a delta of 2 will look at the
                    current month as well as the same month from the last two
                    years.

        budget      :   bool
                    Will also show the budget numbers in accordance to the
                    choices set with FRAME and DELTA.

        deltas      :   list [pandas.DataFrame]
                    A variable amount (based on DELTA) of DataFrames for the
                    chosen FRAMEs and category list in the inserted
                    transactions.

        budgets     :   list [pandas.DataFrame]
                    A variable amount (based on DELTA) of DataFrames for the
                    budgets of the chosen FRAMEs and category list in the
                    inserted transactions.

        Attributes
        ----------
        '''
        # Gets the data for the previous deltas/frames provided in order to
        # compare to the inserted transaction list.
        deltas, budgets = prev_delta(self.cal, frame, delta, budget)
