#!/usr/bin/env python3

# Capital - Financial cashflow and budgeting software
# Copyright (C) 2019 Savoy

# This file is part of Capital.
#
# Capital is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Capital is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Capital.  If not, see <http://www.gnu.org/licenses/>.

import base.analysis as anal
import base.cleanup as cleanup
import interface.arguments as a
import interface.csv as csv

if __name__ == '__main__':
    cleanup.enter()

    if a.args.which == 'ins':
        ins = anal.Insert(a.args.table, data=a.args.csv, edit=a.args.edit,
                         date=a.args.date, company=a.args.company,
                         detail=a.args.detail, cost=a.args.cost,
                         category=a.args.category, account=a.args.account,
                         inflow=a.args.inflow)

        ins.insert()
        print(anal.current(a.args.account))

    elif a.args.which == 'mod':
        mod = anal.Modify(a.args.date, a.args.table, a.args.account,
                          a.args.index)
        mod.modify(edit=True)
        print(anal.current(a.args.account))

    elif a.args.which == 'out':
        out = anal.Output(a.args.date, account=a.args.account)
        print(anal.current(a.args.account))
        if a.args.trans == 0:
            print(out.summary(detail=a.args.detail))
        else:
            print(out.trans(n=a.args.trans))

    cleanup.exit()

    # -v for verbose stuff maybe?
    # --verify that checks the DB after insertion against the saved state to
    # make sure the entries have been loaded
