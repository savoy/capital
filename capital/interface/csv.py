#!/usr/bin/env python3

# Capital - Financial cashflow and budgeting software
# Copyright (C) 2019 Savoy

# This file is part of Capital.
#
# Capital is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Capital is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detail.
#
# You should have received a copy of the GNU General Public License
# along with Capital.  If not, see <http://www.gnu.org/licenses/>.

import os
import pandas as pd
from subprocess import call
import tempfile

def insert(table: str) -> pd.DataFrame:
    '''Opens a temporary CSV file to edit with new transaction data.

    Parameters
    ----------
    table       :   str
                The name of the table name to insert into the database.

    Returns
    -------
    df          :   pandas.DataFrame
                Data to be inserted into the database.

    '''
    if table.lower() == 'inflow':
        init = 'date,company,detail,cost,category,account'
    elif table.lower() == 'debit':
        init = 'date,company,detail,cost,category,account,inflow'

    # Initializes the temporary file with the correct header values
    tf = tempfile.NamedTemporaryFile(mode='w', suffix='.csv', delete=False)
    tf.write(init)
    tf.close()

    # Calls the $EDITOR value to open the CSV in; defaults to vi otherwise.
    prog = os.environ.get('EDITOR', 'vi')
    call([prog, tf.name])

    df = pd.read_csv(tf.name)

    os.unlink(tf.name)
    return df

def modify(data: pd.DataFrame) -> pd.DataFrame:
    '''Opens a temporary CSV file to modify previous data.

    Parameters
    ----------
    data        :   pd.DataFrame
                The data to edit.

    Returns
    -------
    df          :   pandas.DataFrame
                Data to be re-inserted into the database.

    '''
    # Initializes the CSV file with the passed data to modify.
    tf = tempfile.NamedTemporaryFile(mode='w', suffix='.csv', delete=False)
    data.to_csv(tf, index=False)
    tf.close()

    # Calls the $EDITOR value to open the CSV in; defaults to vi otherwise.
    prog = os.environ.get('EDITOR', 'vi')
    call([prog, tf.name])

    df = pd.read_csv(tf.name)

    os.unlink(tf.name)
    return df
