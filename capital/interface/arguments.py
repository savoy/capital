#!/usr/bin/env python3

# Capital - Financial cashflow and budgeting software
# Copyright (C) 2019 Savoy

# This file is part of Capital.
#
# Capital is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Capital is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more detail.
#
# You should have received a copy of the GNU General Public License
# along with Capital.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import datetime as dt
import base.dates as dates

git = 'https://gitlab.com/SavoyRoad/capital'
parser = argparse.ArgumentParser(
    formatter_class = argparse.RawTextHelpFormatter,
    description = 'Budgeting and cash flow software.',
    epilog = 'Source code and developer info can be found here:'
            f'\n{git}'
)
subparsers = parser.add_subparsers(
    description = 'Valid sub-commands',
    help = '-h for detailed help'
)

parser_insert = subparsers.add_parser('insert',
                    help = 'Inserts a transaction(s) into Capital.'
                                      )
parser_insert.set_defaults(which='ins')

parser_insert.add_argument('table',
                           nargs='?',
                           help = (
                               'Type of transaction to enter (Inflow vs Debit).'
                           ))
parser_insert.add_argument('-e', '--edit',
                           action='store_true',
                           help = (
                               'Opens your $EDITOR to interactively insert '
                               'transactions.'
                           ))
parser_insert.add_argument('-c', '--csv',
                           nargs='?',
                           help = (
                               'Path to CSV of transactions to insert in lieu '
                                'of manual command line entry.'
                           ))
parser_insert.add_argument('--date',
                           nargs='+',
                           help = 'List of transaction dates to be entered.'
                           )
parser_insert.add_argument('--company',
                           nargs='+',
                           help = 'List of transaction companies to be entered.'
                           )
parser_insert.add_argument('--detail',
                           nargs='+',
                           help = 'List of transaction details to be entered.'
                           )
parser_insert.add_argument('--cost',
                           nargs='+',
                           help = 'List of transaction costs to be entered.'
                           )
parser_insert.add_argument('--category',
                           nargs='+',
                           help = 'List of transaction categories to be entered.'
                           )
parser_insert.add_argument('--account',
                           nargs='+',
                           help = 'List of transaction accounts to be entered.'
                           )
parser_insert.add_argument('--inflow',
                           nargs='+',
                           help = 'List of inflow accounts if trans are debits.'
                           )

parser_modify = subparsers.add_parser('modify',
                    help = 'Modifies a transaction(s) in Capital.'
                                      )
parser_modify.set_defaults(which='mod')

parser_modify.add_argument('date',
                           nargs='?',
                           help = (
                               'Date range of transactions to modify.'
                           ))
parser_modify.add_argument('table',
                           nargs='?',
                           help = (
                               'Type of transaction to enter (Inflow vs Debit).'
                           ))
#parser_modify.add_argument('-e', '--edit',
#                           action='store_true',
#                           help = (
#                               'Opens your $EDITOR to interactively edit '
#                               'transactions.'
#                           ))
parser_modify.add_argument('--account',
                           nargs='?',
                           help = 'The transaction account to be pulled. '
                           'No entry will default to the first account listed '
                           'in the configuration file.'
                           )
parser_modify.add_argument('--index',
                           nargs='*',
                           help = 'List of transaction IDs to modify.'
                           )

parser_output = subparsers.add_parser('output',
                    help = 'View summations of balances and transactions.'
                                      )
parser_output.set_defaults(which='out')

parser_output.add_argument('-d', '--detail', action='store_true',
                           help = (
                               'Prints a detailed summary of transactions.')
                           )
parser_output.add_argument('-t', '--trans', default=0, type=int,
                           help = (
                               'Prints a given amount of transaction lines '
                               'as opposed to a summation.')
                           )
parser_output.add_argument('--date',
                           default=str(dates.calendar(dt.date.today().year,
                                                  ref=dt.date.today())[2]),
                           nargs='?',
                           help = 'Date frame of output to view.'
                           )
parser_output.add_argument('--account',
                           nargs='*',
                           help = 'List of transaction accounts to be pulled. '
                           'No entry will default to the first account listed '
                           'in the configuration file.'
                           )

args = parser.parse_args()
#insert_args = [args.date, args.company, args.detail, args.cost,
#        args.category, args.account]
