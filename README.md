# capital
###### or the lack thereof...
`capital` grew out of a desire for a simpler way to keep track of ones money
without the need of any prior accounting knowledge, powerful but
unnecessary software, or any desktop GUI dependencies. It was designed for
proles, for people who want or need to keep better track of their finances but
don't require the advanced functionality of software like
[ledger](https://www.ledger-cli.org/), [GnuCash](https://gnucash.org/), or want
to limit their use of online software ([Mint](https://www.mint.com/) comes to
mind).

### Features
The three main workflow aspects of `capital` are inserting new transactions
into the database, modifying existing data (including keeping track of any
potential credit card payments), and outputting previous transactions as raw
data or in different summary forms. The main front-end is through the command
line with two forms (for insert and modify): either through traditional
command line arguments or through an "interactive" CSV data input for easier
insertion/modification of multiple transactions.

What will be coming in a future update will be allowing `capital` to run as a
"server" with a front-end bot through a messaging protocol (XMPP first, potentially
Telegram later). This will allow for a quick way to view your current balance
or insert transactions while away from your computer.

### Usage 
##### Insert
`capital insert <inflow OR debit> -e` will open a CSV in the program specified
by your $EDITOR environmental variable, pre-formatted with the columns required
to populate. The `category`, `account`, and if inserting a debit, `inflow`
columns must be filled in and match to items you have in your config. Before
the transactions are entered, they are cleaned-up and verified to match them to
your config.
![alt_tag](capital/screenshots/insert_csv.png?raw=true)

##### Modify
`capital modify <date> <inflow OR debit>` will open a CSV in the program
specified by $EDITOR, outputting all transactions filtered by the CLI
arguments. Additionally, `--account` and `--index` can be used to choose a
different account other than the config default while `index` allows for
selecting specific transaction IDs to modify without bringing up more into
memory.

##### Output
`capital output` will default to printing the current account balances net any
outstanding credit costs, along with a summarized look at transactions by their
category `header`, for the current datetime year and for the first inflow
account listed in your config (modifiable by the `--date` and `--account`
arguments). The `-d` detail argument will flag to show that previous summary by both
`header` and `category`, while `-t N` will show instead a list of *N*
most transactions.

### Configuration
A sample configuration is available under `capital/template_config.yaml`.
First-run of the program will require `capital init` to copy the config file
and prepare the working state required. The structure of the file must contain:

* datetime: The calendar style to use (`standard` or `financial`). Financial
    differs in that it standardizes months to match in days - months 3, 6, 9,
    12 contain 5 weeks/34 days (some Decembers contain 6 weeks if the following
    year would start too late), while the others contain 4 weeks/27 days - as
    well as what days they start; all months start on Sundays and end 4/5/6
    weeks later on Saturday.
* database: Contains sub-entries for the database `name` as well as its
    `location`. Any changes to the filepath of the database should be done here
    first. Upon program startup, `capital` will note the change in the config
    file and move the database to its new location.
* account: Contains the `inflow`, `debit`, and `credit` sub-entries. `inflow`
    will contain what would be considered your bank account(s), `debit` should
    contain an entry per `inflow` account, along with any credit cards owned,
    while `credit` should contain only the credit cards owned or similar lines
    of credit to track.
* category: Two levels of sub-sections. The top levels split up the `inflow`
    section for transaction categories that you bring in as revenue, while two
    more, `constant` and `variable`, categorize transactions that are
    repeatable (rent, bills, food), or that would potentially vary more month
    to month or are less consistent (entertainment, clothes, household things),
    respectively.

### Installation
This project uses [poetry](https://poetry.eustace.io/) for dependency
management in a virtual environment. Install `poetry` and do the following:
```
git clone https://gitlab.com/SavoyRoad/capital
cd capital
poetry install
```
Once the dependencies located in `pyproject.toml` have been installed in the
virtual environment, `capital` can be run in two ways:
```
poetry run ./capital <arguments>
# OR
poetry shell
./capital <arguments>
```

Inclusion in `pip` will occur once more feature-complete.
